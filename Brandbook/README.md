# Brandbook



This directory currently contains the UBports brandbook.
This brandbook defines the current styling of the project.
It is the result of the efforts of many individuals.
The initial version contained a lot of closed license content. This has been rectified as much as possible.
It was converted into a Scribus file.

In the contentpack directory are it's source files.
Due to some Scribus size limitations those have been split in three parts.
Once these Scribus bugs are fixed it will become one file.






